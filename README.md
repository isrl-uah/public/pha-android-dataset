# PHA-Android-Dataset

Malware detection inside App Storesbased on lifespan measurements paper dataset

## Citation

```
@ARTICLE{9522135,
  author={Cilleruelo, Carlos and Enrique-Larriba and De-Marcos, Luis and Martinez-Herráiz, Jose-Javier},
  journal={IEEE Access}, 
  title={Malware detection inside App Stores based on lifespan measurements}, 
  year={2021},
  volume={},
  number={},
  pages={1-10},
  doi={10.1109/ACCESS.2021.3107903}}
```
